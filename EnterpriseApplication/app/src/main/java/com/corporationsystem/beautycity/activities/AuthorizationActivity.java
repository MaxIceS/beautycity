package com.corporationsystem.beautycity.activities;

import android.app.Activity;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.corporationsystem.beautycity.models.SignInModel;
import com.maxices.portalclient.R;

import java.util.ArrayList;
import java.util.List;


public class AuthorizationActivity extends Activity implements LoaderCallbacks<Cursor>, SignInModel.Observer {

    private static final String TAG = "AuthorizationActivity";

    Animation shakeanimation;

    // UI references.
    private EditText mLoginView;
    private EditText mPasswordView;
    private View mProgress;
    private View mLoginFormView;
    private Button mEmailSignInButton;

    private SignInModel mSignInModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorization);

        // Set up the login form.
        mLoginView = (EditText) findViewById(R.id.view_login);
        populateAutoComplete();

        shakeanimation = AnimationUtils.loadAnimation(this, R.anim.shake);

        mPasswordView = (EditText) findViewById(R.id.view_password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.view_login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mEmailSignInButton = (Button) findViewById(R.id.button_sign_in);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.view_login);
        mProgress = findViewById(R.id.view_progress);

        mSignInModel = new SignInModel();
        mSignInModel.registerObserver(this);
    }

    private void populateAutoComplete() {
        getLoaderManager().initLoader(0, null, this);
    }


    public void attemptLogin() {
//        if (mAuthTask != null) {
//            return;
//        }

        // Reset errors.
        mLoginView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String login = mLoginView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;


        // Check for a valid password, if the user entered one.
        if (!isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            mPasswordView.startAnimation(shakeanimation);
            cancel = true;
        }
        if(TextUtils.isEmpty(password)){
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            mPasswordView.startAnimation(shakeanimation);
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
//            showProgress(true);
//            mAuthTask = new UserLoginTask(email, password);
//            mAuthTask.execute((Void) null);
            mSignInModel.signIn(login, password);

        }
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<String>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }
    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG, "onDestroy");
        super.onDestroy();
        mSignInModel.unregisterObserver(this);

        if (isFinishing()) {
            mSignInModel.stopSignIn();
        }
    }

    private void showProgress(final boolean show) {
        mLoginFormView.setEnabled(!show);
        mPasswordView.setEnabled(!show);
        mEmailSignInButton.setEnabled(!show);
        mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
    }


    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    @Override
    public void onSignInStarted(SignInModel signInModel) {
        Log.i(TAG, "onSignInStarted");
        showProgress(true);
    }

    @Override
    public void onSignInSucceeded(SignInModel signInModel) {
        Log.i(TAG, "onSignInSucceeded");
        saveToken();
        finish();
        startActivity(new Intent(this, CorporateClientActivity.class));
    }

    @Override
    public void onSignInFailed(SignInModel signInModel) {
        Log.i(TAG, "onSignInFailed");
        showProgress(false);
        Toast.makeText(this, R.string.sign_in_error, Toast.LENGTH_SHORT).show();
    }

    private void saveToken(){
        SharedPreferences sharedPref = getSharedPreferences("Authorization", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("Token", "");
        editor.commit();
    }
}