package com.corporationsystem.beautycity;

import java.util.HashMap;

/**
 * Created by Максимилиан on 11.05.2015.
 * для List frag1
 */
public class ListFragA extends HashMap<String, String> {

    public static final String NUMBER = "number";
    public static final String NAME = "name";

    // Конструктор с параметрами
    public ListFragA(String number, String name) {
        super();
        super.put(NUMBER, number);
        super.put(NAME, name);
    }
}