package com.corporationsystem.beautycity.commutator;


public class BackendCommunicatorStub implements BackendCommunicator{
    @Override
    public boolean postSignIn(String userName, String password) throws InterruptedException {
        Thread.sleep(8000);
        return true;
    }
}
