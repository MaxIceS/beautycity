package com.corporationsystem.beautycity.activities;

import android.app.Fragment;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.corporationsystem.beautycity.ListFragA;
import com.corporationsystem.beautycity.fragments.AllPropertiesFragment;
import com.corporationsystem.beautycity.fragments.TwoFragment;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.maxices.portalclient.R;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    Button frag1btn1; //добавить
    Button frag1btn2; //изменить
    Button frag1btn3; //удалить


    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        frag1btn1 = (Button)findViewById(R.id.frag1btn1);
        frag1btn2 = (Button)findViewById(R.id.frag1btn2);
        frag1btn3 = (Button)findViewById(R.id.frag1btn3);

        frag1btn1.setOnClickListener(this);
        frag1btn2.setOnClickListener(this);
        frag1btn3.setOnClickListener(this);

        ListView listView = (ListView)findViewById(R.id.listView);

        ArrayList<ListFragA> list = new ArrayList<ListFragA>();
        // Заполняем данными
        list.add(new ListFragA("123", "Газонное ограждение"));
        list.add(new ListFragA("124", "Пешеходное ограждение"));
        list.add(new ListFragA("125", "Силовое ограждение"));
        list.add(new ListFragA("126", "Забор"));
        list.add(new ListFragA("127", "Ворота"));
        list.add(new ListFragA("128", "Калитка"));

        ListAdapter adapter = new SimpleAdapter(this, list, R.layout.item_layout,
                new String[] { ListFragA.NUMBER, ListFragA.NAME }, new int[] {
                R.id.textViewItemNumber, R.id.textViewItemNames });
        listView.setAdapter(adapter);

    }

    @Override
    public void onClick(View v) {
        Fragment fragment = new Fragment();
        switch (v.getId()) {
            case R.id.frag1btn1:
                fragment = new TwoFragment();
                break;
            case R.id.frag1btn2:

                break;
            case R.id.frag1btn3:

                break;
        }
        getFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }


    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link com.google.android.gms.maps.SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
    }
}
