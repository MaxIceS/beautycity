package com.corporationsystem.beautycity.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;
import com.maxices.portalclient.R;

public class CorporateClientActivity extends ActionBarActivity implements OnClickListener{

    Button act2btn1map;
    Button act2btn2synchronization;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_corporate_client);

        act2btn1map = (Button)findViewById(R.id.act2btn1map);
        act2btn2synchronization = (Button)findViewById(R.id.act2btn2synchronization);

        act2btn1map.setOnClickListener(this);
        act2btn2synchronization.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPref = getSharedPreferences("Authorization", Context.MODE_PRIVATE);
        if(!sharedPref.contains("Token")){
            finish();
            startActivity(new Intent(this, AuthorizationActivity.class));
        }
        else {
            String token = sharedPref.getString("Token", "");
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.act2btn1map:
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
            case R.id.act2btn2synchronization:

                break;
        }
    }
}
