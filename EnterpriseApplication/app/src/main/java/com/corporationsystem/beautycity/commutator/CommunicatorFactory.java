package com.corporationsystem.beautycity.commutator;


public class CommunicatorFactory {

    public static BackendCommunicator createBackendCommunicator() {
        return new BackendCommunicatorStub();
    }
}
