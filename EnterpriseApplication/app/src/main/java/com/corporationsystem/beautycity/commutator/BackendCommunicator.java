package com.corporationsystem.beautycity.commutator;


public interface BackendCommunicator {
    boolean postSignIn(String userName, String password) throws InterruptedException;
}
